import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Library{
	private List<Member> memberList = new ArrayList<Member>();

	public List<Member> getMemberList() {
		return memberList;
	}

	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}  
	
	public void addMember(Member memberObj) 
	{
		memberList.add(memberObj);
	}
	public List<Member> viewAllMembers()
	{
		return memberList;
		
		
	}
	public List<Member> viewMembersByAddress(String address) 
	{
		List<Member> mem3 = new ArrayList<Member>();
		Iterator<Member> mem2 = memberList.iterator();
		while(mem2.hasNext())
		{
			Member m = mem2.next();
			if(address.equals(m.getAddress()))
					{
				mem3.add(m);
					}
			
		}
		return mem3;
		
	}
	
}