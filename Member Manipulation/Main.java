import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Main{
	
	

public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	boolean flag=true;
	Library l =new Library();
	while(flag) {
	
	System.out.println("1. Add Member");
	System.out.println("2. View All Member");
	System.out.println("3. Search Member by address");
	System.out.println("4. Exit");
	System.out.println("Enter your choice");
	
	int opt = sc.nextInt();
	
	switch(opt)
	{
	case 1:
	{
		System.out.println("Enter the id:");
		int id=sc.nextInt();
		System.out.println("Enter the name:");
		String name =sc.next();
		System.out.println("Enter the address:");
		String address=sc.next();
		Member m = new Member(id,name,address);
		l.addMember(m);
		
		break;
	}
	case 2:
	{
		List<Member> memberList = l.viewAllMembers();  
		Iterator<Member> mem = memberList.iterator();
		while(mem.hasNext()) 
		{
			
			Member mem1 = mem.next();
			System.out.println("Id:"+mem1.getMemberId()+"\nName:"+mem1.getMemberName()+"\nAddress:"+mem1.getAddress());
		}
		break;
		
	}
	case 3:
	{
		System.out.println("Enter the address");
		String address = sc.next();
		List<Member> mem2 = l.viewMembersByAddress(address);
		Iterator<Member> mem = mem2.iterator();
		while(mem.hasNext()) 
		{
			Member mem1 = mem.next();
			System.out.println("Id:"+mem1.getMemberId()+"\nName:"+mem1.getMemberName()+"\nAddress:"+mem1.getAddress());
		}
		break;
		
	}
	
	case 4:
	{
		flag=false;
		break;
	}
	}
}
}
}