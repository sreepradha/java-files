import java.util.Scanner;

public class Main{
	
	public static void main(String[] args) 
	{
		Hosteller s = getHostellerDetails();
		
		System.out.println("The Student Details"+s.getStudentId()+" "+s.getName()+" "+s.getDepartmentId()+" "+s.getGender()+" "+s.getPhone()+" "+s.getHostelName()+" "+s.getRoomNumber());
	}
	
	public static Hosteller getHostellerDetails() {
		Scanner sc = new Scanner(System.in);
		Hosteller s = new Hosteller();
		System.out.println("Enter the Details:");
		System.out.println("Student Id");
		int id = Integer.parseInt(sc.nextLine());
		System.out.println("Student Name");
		String name = sc.nextLine();
		System.out.println("Department Id");
		int departmentid = Integer.parseInt(sc.nextLine());
		System.out.println("Gender");
		String gender = sc.nextLine();
		System.out.println("Phone Number");
		String phoneno = sc.nextLine();
		System.out.println("Hostel Name");
		String hostelname = sc.nextLine();
		System.out.println("Room Number");
		int roomno = Integer.parseInt(sc.nextLine());
	
		
		
		
		System.out.println("Modify Room Number(Y/N)");
		
		String mroomno = sc.nextLine();
		if(mroomno.equals("Y")) 
		{
			System.out.println("New Room Number");
			int newroomno = Integer.parseInt(sc.nextLine());
			s.setRoomNumber(newroomno);
		}
		else {
			s.setRoomNumber(roomno);
		}
		System.out.println("Modify Phone Number(Y/N)");
		String mphoneno = sc.nextLine();
		
		if(mphoneno.equals("Y")) 
		{
			System.out.println("New Phone Number");
			String newphoneno = sc.nextLine();
			s.setPhone(newphoneno);
		}
		else {
			s.setPhone(phoneno);
		}
		
		s.setStudentId(id);
		s.setName(name);
		s.setDepartmentId(departmentid);
		s.setGender(gender);
		s.setHostelName(hostelname);
	
		
		
		
		
		return s;
	}
}