import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Main {
     public static void main(String as[]) throws IOException {
          BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
          String currentTime = "29/10/2019";
          System.out.println("Recharged date");
          String recharge = br.readLine();
          LocalDate ld = null;
          try {
              DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
              LocalDate rech = LocalDate.parse(recharge, format);
              LocalDate curt = LocalDate.parse(currentTime, format);
              if (rech.isAfter(curt))
                   System.out.println("Invalid date");
              else {
                   System.out.println("Validity days");
                   long day = Integer.parseInt(br.readLine());
                   if (day <= 0)
                        System.out.println("Invalid days");
                   else {
                        ld = rech.plusDays(day);
                        System.out.println(ld.format(format));
                   }
              }
          } catch (DateTimeParseException d) {
              System.out.println("Invalid date");
          }
     }
}


