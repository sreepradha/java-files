package com.spring.app;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Driver {
	
public static void main(String[] args)
{
	
	Scanner sc = new Scanner(System.in);

	
	System.out.println("Enter the cus id:");
	int cusId = sc.nextInt();
	System.out.println("Enter the cus name:");
	String cusName = sc.next();
	System.out.println("Enter the email id:");
	String emailId = sc.next();
	System.out.println("Enter the contact no:");
	long contactNo = sc.nextLong();
	
	
	Customer c = new Customer(cusId,cusName,emailId,contactNo);
	
	
	System.out.println("Enter the membership id:");
	int membershipId = sc.nextInt();
	System.out.println("Enter the membershiptype:");
	String membershipType = sc.next();
	System.out.println("Enter the visit per year:");
	int visitsPerYear = sc.nextInt();

	@SuppressWarnings("resource")
	ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
	MemberShip a = (MemberShip) ac.getBean("custObj1");
	a = new MemberShip(membershipId,membershipType,visitsPerYear,c);
	System.out.println(a);
	sc.close();
	
	
}

}
