package com.app.driver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.app.EmployeeDAO;



public class Main {
 
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ApplicationContext a = new ClassPathXmlApplicationContext("beans.xml");
		EmployeeDAO e = (EmployeeDAO) a.getBean("db1");	
		e.show();
 
 
		
	}
}