package com.spring.app;

public class AddressBook {
	private String phoneNumber;
	private Address tempAddress;
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public Address getTempAddress() {
		return tempAddress;
	}
	public void setTempAddress(Address tempAddress) {
		this.tempAddress = tempAddress;
	}
	public AddressBook(String phoneNumber, Address tempAddress) {
		super();
		this.phoneNumber = phoneNumber;
		this.tempAddress = tempAddress;
	}
	public AddressBook() {
		
	}
	public void show() {
		System.out.println("Temporary address");
		System.out.println("House name:"+tempAddress.getHouseName());
		System.out.println("Street:"+tempAddress.getStreet());
		System.out.println("City:"+tempAddress.getCity());
		System.out.println("State:"+tempAddress.getState());
		System.out.println("Phone number:"+phoneNumber);

	}
	



}
