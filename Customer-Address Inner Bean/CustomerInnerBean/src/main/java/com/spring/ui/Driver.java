package com.spring.ui;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.app.Address;
import com.spring.app.AddressBook;


public class Driver {
	
	
	public static AddressBook loadAddressBook()
	{
		ApplicationContext a = new ClassPathXmlApplicationContext("applicationContext.xml");
		AddressBook e = (AddressBook) a.getBean("ab1");	
		return e;
	}
	
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the temporary address");
		System.out.println("Enter the house name");
		String housename = sc.nextLine();
		System.out.println("Enter the street");
		String street =  sc.nextLine();
		System.out.println("Enter the city");
		String city = sc.nextLine();
		System.out.println("Enter the state");
		String state = sc.nextLine();
		System.out.println("Enter the phone number");
		String phno = sc.nextLine();
		
		Address a = new Address(housename,street,city,state);
		AddressBook ab = loadAddressBook();
		ab =new AddressBook(phno,a);
	ab.show();
		//invoke the loadAddressBook() method from main retrieve the AddressBook object, get the details from the user set the values and display the values

	}

}
