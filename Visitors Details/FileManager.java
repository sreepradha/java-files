import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

//import necessary packages
 
 @SuppressWarnings("unchecked")//Do not delete this line
 public class FileManager 
 {
    
    static public File createFile()
    {	
    	
    	
    	File f =  new File("visitors.txt");
    	try {
    		f.createNewFile();
    	}
    	catch(IOException e) {
    		System.out.println(e);
    	}
    	return f;
   //change the return type as per the requirement    
    }
    static public void writeFile(File f, String record)
	{
	   try {
		FileOutputStream fos=new FileOutputStream(f,true);
		BufferedOutputStream bos=new BufferedOutputStream(fos);
		bos.write(record.getBytes());
		 bos.close();
		 fos.close();
		 }
	   catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	} 
	static public String[] readFile(File f)
	{String []arr =null;
		try {
			FileInputStream fos = new FileInputStream(f);
			BufferedReader br = new BufferedReader(new InputStreamReader(fos));
			String output;
			
			int i=0;
				while((output=br.readLine())!=null)
				{
					arr=output.split(";");
					
				
				}
				
				


				 br.close();
			        fos.close();
        }
		 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return arr;//change the return type as per the requirement  
	}
 }