import java.util.Scanner;

public class ValidateUtility
{
    public static void main(String args[])
    {
    	Scanner sc = new Scanner(System.in);
    	String empname = sc.nextLine();
    	String pdtname = sc.nextLine();
    	Validate v = validateEmployeeName();
    	Validate v1 = validateProductName();
    
    	
    	
    	if(v.validateName(empname))
    	{
    		System.out.println("Employee name is valid");
    	}
    	else {
    		System.out.println("Employee name is invalid");
    	}
    	if(v1.validateName(pdtname))
    	{
    		System.out.println("Product name is valid");
    	}
    	else {
    		System.out.println("Product name is invalid");
    	}
		
    	}
    
    
    public static Validate validateEmployeeName() 
    {
    
    	Validate v1 = (empname) -> (empname.matches("[A-Za-z ]{5,20}"));    	
    	return v1;
    	
    }
    
    public static Validate validateProductName() 
    {	
    	Validate v = (pdtname) -> (pdtname.matches("[A-Za-z]{1}[0-9]{5}"));
		return v;
        
    }
}