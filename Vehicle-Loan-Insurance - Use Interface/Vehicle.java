
public class Vehicle implements Insurance{
	
	private String vehicleNumber;
	private String modelName;
	private String vehicleType;
	private double price;
	
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public Vehicle(String vehicleNumber, String modelName, String vehicleType,double price) {
		
		this.vehicleNumber = vehicleNumber;
		this.modelName = modelName;
		this.vehicleType=vehicleType;
		this.price = price;
	}
	double discount=0;
	double loan=0;
	double insuranceamt;
	@Override
	public double takeInsurance() {
if(price<=150000) {
	insuranceamt = 3500;
}
else if(price>150000 && price<=300000) {
	insuranceamt = 4000;
}		
else if(price>300000) {
	insuranceamt = 5000;
}
		
		
		
		
		return insuranceamt;
	}
	@Override
	public double issueLoan() {
if(vehicleType.equals("4 wheeler")) {
	discount = (price/100)*80;
	loan = discount;
}
else if(vehicleType.equals("3 wheeler"))	{
	discount = (price/100)*75;
	loan = discount;
}
else if(vehicleType.equals("2 wheeler")){
	discount = (price/100)*50;
	loan = discount;
	
}
		
		return loan;
	}

}
