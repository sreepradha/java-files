import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DBHandler {

public Connection establishConnection() throws Exception{
		
	FileReader myfile = new FileReader("db.properties");
    Properties props = new Properties();
    props.load(myfile);
    String url = props.getProperty("db.url");
    String driver = props.getProperty("db.classname");
    String user = props.getProperty("db.username");
    String password = props.getProperty("db.password");
    Class.forName(driver);
    Connection conn = DriverManager.getConnection(url,user,password);
    return conn;  
}
}