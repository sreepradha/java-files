public class Package {
	private String packageId;
	private String sourcePlace;
	private String destinationPlace;
	private double basicFare;
	private int noOfDays;
	private double packageCost;
	
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getSourcePlace() {
		return sourcePlace;
	}
	public void setSourcePlace(String sourcePlace) {
		this.sourcePlace = sourcePlace;
	}
	public String getDestinationPlace() {
		return destinationPlace;
	}
	public void setDestinationPlace(String destinationPlace) {
		this.destinationPlace = destinationPlace;
	}
	public double getBasicFare() {
		return basicFare;
	}
	public void setBasicFare(double basicFare) {
		this.basicFare = basicFare;
	}
	public int getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}
	public double getPackageCost() {
		return packageCost;
	}
	public void setPackageCost(double packageCost) {
		this.packageCost = packageCost;
	}

    //write the required business logic methods as expected in the question description
	public void calculatePackageCost() {
	
		packageCost = noOfDays*basicFare;
		   if(noOfDays>5 && noOfDays<=8)
		   {    
			   packageCost-=(packageCost*(0.03)); 
			   }
		   if(noOfDays>8 && noOfDays<=10)
		   {   
			   packageCost-=(packageCost*(0.05)); 
			   }
		   if(noOfDays>10)
		   {   
			   packageCost-=(packageCost*(0.07));
			   }
		   packageCost+=(packageCost*0.12);
		   
		   this.setPackageCost(packageCost);
	}
}
