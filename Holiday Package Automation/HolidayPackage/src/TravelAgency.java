import java.io.*;
import java.util.ArrayList;
import java.util.*;
import java.sql.*;


public class TravelAgency {
    
    //write the required business logic methods as expected in the question description
	
	public List<Package> generatePackageCost(String filePath)
	{
		List<Package> p = new ArrayList<Package>();
	    try 
	     {  File f =  new File(filePath);
	        Scanner s = new Scanner(f);
	        while(s.hasNextLine())
	        {  String[] arr = (s.nextLine()).split(",",0);
	           try 
	            {  
    	           if(validate(arr[0])==true)
    	            {  Package ob = new Package();
    	               ob.setPackageId(arr[0]);
    	               ob.setSourcePlace(arr[1]);
    	               ob.setDestinationPlace(arr[2]);
    	               ob.setBasicFare(Double.parseDouble(arr[3]));
    	               ob.setNoOfDays(Integer.parseInt(arr[4]));
    	               ob.calculatePackageCost();
    	               p.add(ob);
    	            }
	            }
	           catch(InvalidPackageIdException ex)
	            { System.out.println(ex); }
	        }
	     }
	     catch(Exception ex)
	     {  ex.printStackTrace(); }
	   return p;
	}
	
	public boolean validate(String packageId) throws InvalidPackageIdException {
		
		boolean k=true;
		if((packageId.length()!=7) || (packageId.indexOf("/")!=3))
		{
		  throw new InvalidPackageIdException("Invalid Package Id");
		}
		return k;
    	
	}
public List<Package> findPackagesWithMinimumNumberOfDays() {
	List<Package> ob_list = new ArrayList<Package>();
	try 
	  { 
	    DBHandler obj = new DBHandler();
	    Connection conn = obj.establishConnection();
	    Statement smt = conn.createStatement();
	    String sql = "select * from package_details where no_of_days in (select min(temp) from (select no_of_days as temp from package_details as q) as a);";
	    ResultSet rs = smt.executeQuery(sql);
	    while(rs.next())
	    {   Package object =  new Package();
	        object.setPackageId(rs.getString("package_id"));
            object.setSourcePlace(rs.getString("source_place"));
            object.setDestinationPlace(rs.getString("destination_place"));
            object.setNoOfDays(rs.getInt("no_of_days"));
            object.setPackageCost(rs.getDouble("package_cost"));
            ob_list.add(object);
	    }
	  }
	  catch(Exception ex)
	  {  ex.printStackTrace(); }
   return ob_list;
}

}
