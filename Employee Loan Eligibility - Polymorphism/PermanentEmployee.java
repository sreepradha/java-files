//Make this class inherit the Employee class

public class PermanentEmployee extends Employee
{
    private double basicPay;
    
    // Getters and Setters
	
    public double getBasicPay() {
		return basicPay;
	}

	public void setBasicPay(double basicPay) {
		this.basicPay = basicPay;
	}


	public void calculateSalary() {
		super.salary = basicPay - ( basicPay*0.12);
	}

	public PermanentEmployee(int employeeid, String employeename, double basicPay) {
		super(employeeid, employeename);
		this.basicPay = basicPay;
	}
    
    //1. Write a public 3 argument constructor with arguments – employeeId, employeeName and basicPay.  
    
    
    
	
	//2. Implement the  - public void calculateSalary() - method   
	
    
    
}