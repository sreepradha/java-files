package com.utility;

import java.util.ArrayList;
import java.util.List;

public class GPACalculator {
	
    private List<Integer> gradePointList;
	
	
	public List<Integer> getGradePointList() {
		return gradePointList;
	}


	public void setGradePointList(List<Integer> gradePointList) {
		this.gradePointList = gradePointList;
	}


    /*This method should add equivalent grade points based on the grade obtained by the student passed as argument into gradePointList
	  Grade	        S	A	B	C	D	E
	  Grade Point	10	9	8	7	6	5
	  For example if the gradeobtained is A, its equivalent grade points is 9 has to added into the gradePointList*/
	public void addGradePoint(char gradeObtained) {
		int point=0;
		
		switch(gradeObtained)
				{
			case 'S':
				point = 10;
				break;
			case 'A':
				point = 9;
				break;
			case 'B':
				point = 8;
				break;
			case 'C':
				point = 7;
				break;
			case 'D':
				point = 6;
				break;
			case 'E':
				point = 5;
				break;
				}
		gradePointList.add(point);
	}
	
	
	/* This method should return the GPA of all grades scored in the semester 
	 GPA  can be calculated based on the following formula
	 GPA= (gradePoint1 + gradePoint2 + ... + gradePointN) / (size of List)
	
	For Example:
	 if the list contains the following marks [9,10,8,5]
	 GPA = (9 + 10 + 8 + 5) / (4)= 8.0

	 */
	public double calculateGPAScored() {
	    
			double gpa=-1;
			if(gradePointList.size()>0)
			{
				double sum=0;
				for(int i=0;i<gradePointList.size();i++)
				{
					sum += gradePointList.get(i);				
					}
			
			
			gpa= sum  /gradePointList.size();
			return gpa;
	}
			else {
				return 0;
			}
			// fill the code
			
			
	}
}
