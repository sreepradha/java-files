package com.spring.app;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Driver {
	
public static void main(String[] args)
{
Scanner sc = new Scanner(System.in);
String str = sc.nextLine();
ApplicationContext bean  = new ClassPathXmlApplicationContext("beans.xml");
CurrencyConverter c = (CurrencyConverter) bean.getBean("cc");
System.out.println(c.getTotalCurrencyValue(str));
	
}

}
