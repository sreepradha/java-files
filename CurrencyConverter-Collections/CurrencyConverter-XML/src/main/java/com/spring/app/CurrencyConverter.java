package com.spring.app;

import java.util.HashMap;
import java.util.Map;

public class CurrencyConverter {
	private Map<String, String> mObj;
	
	    public Map<String, String> getmObj() {
		return mObj;
	}

	public void setmObj(Map<String, String> mObj) {
		this.mObj = mObj;
	}

		public int getTotalCurrencyValue(String value)
    	{
			value = value.toUpperCase();
			String regex = "[0-9]+[A-Z]+";
			String currency = "";
			String val = "";
			if(value.matches(regex)) 
			{
				for(int i=0;i<value.length();i++)
				{
					if(value.charAt(i)>='A' && value.charAt(i)<='Z') 
					{
						currency+=value.charAt(i);
					}
					else {
						val+=value.charAt(i);
					}
				}
			}
	        return Integer.parseInt(val)*Integer.parseInt(mObj.get(currency));
	 
	    }	
	
		    	    	     	      	 	

}
