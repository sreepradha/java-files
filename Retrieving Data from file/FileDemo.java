import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileDemo
{
    public static void main(String[] args)
    {
        try {
			FileInputStream fos = new FileInputStream("log.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(fos));
			String output;
			
				while((output=br.readLine())!=null){
					System.out.println(output);
				}


				 br.close();
			        fos.close();
			        

			
        }
		 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
    }
}