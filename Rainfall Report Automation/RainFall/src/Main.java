import java.util.List;

public class Main {
   public static void main(String[] args) { 
	   
	   RainfallReport rainfallReport=new RainfallReport();
	   
	   List<AnnualRainfall> l1=rainfallReport.generateRainfallReport("C:\\Internship_Java\\eclipse\\Tekstac\\Rainfall Report Automation\\RainFall\\src\\AllCityMonthlyRainfall.txt");
	   
	   System.out.println(l1);
	   
	   List<AnnualRainfall> l2=rainfallReport.findMaximumRainfallCities();
	   System.out.println(l2);
   }
}