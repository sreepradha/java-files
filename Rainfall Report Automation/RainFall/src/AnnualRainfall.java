public class AnnualRainfall {
	
	
	private int cityPincode;
	private String cityName;
	private double averageAnnualRainfall;

	public int getCityPincode() {
		return cityPincode;
	}

	public void setCityPincode(int cityPincode) {
		this.cityPincode = cityPincode;
	}

	public String getCityName(){
		return cityName;
	}

	public void setCityName(String cityName){
		this.cityName = cityName;
	}


	public double getAverageAnnualRainfall(){
		return averageAnnualRainfall;
	}


	public void setAverageAnnualRainfall(double averageAnnualRainfall){
		this.averageAnnualRainfall = averageAnnualRainfall;
	}

	@Override
	public String toString() {
		return "AnnualRainfall [cityPincode=" + cityPincode + ", cityName=" + cityName + ", averageAnnualRainfall="
				+ averageAnnualRainfall + "]";
	}

	//Write the required business logic as expected in the question description
	public void calculateAverageAnnualRainfall (double monthlyRainfall [ ]){
	    
		int len=monthlyRainfall.length;
		double sum=0.0;
		for(int i=0;i<len;i++)
			sum+=monthlyRainfall[i];
		setAverageAnnualRainfall((sum/(double)len));
	}


}