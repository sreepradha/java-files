import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RainfallReport {

	// Write the required business logic as expected in the question description
	public List<AnnualRainfall> generateRainfallReport(String filePath) {

		RainfallReport rainfallReport = new RainfallReport();
		File file = new File(filePath);
		List<AnnualRainfall> list = new ArrayList<AnnualRainfall>();
		double report[] = new double[12];
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				int j = 0;
				String dummy[] = line.split(",");
				if (rainfallReport.validate(dummy[j])) {
					AnnualRainfall annualRainfall = new AnnualRainfall();
					annualRainfall.setCityPincode(Integer.parseInt(dummy[j++]));
					annualRainfall.setCityName(dummy[j++]);
					for (int i = 0; i < 12; i++)
						report[i] = Double.parseDouble(dummy[j++]);
					annualRainfall.calculateAverageAnnualRainfall(report);
					list.add(annualRainfall);
					line = "";
				}
			}
			System.out.println("data inserted successfully");
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidCityPincodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public List<AnnualRainfall> findMaximumRainfallCities() {
		
		List<AnnualRainfall> list=new ArrayList<AnnualRainfall>();
		
		
		DBHandler db=new DBHandler();
		try {
			
			Connection con=db.establishConnection();
			Statement smt=con.createStatement();
			String query="select * from AnnualRainfall where average_annual_rainfall = (select max(average_annual_rainfall) from AnnualRainfall);";
			
			ResultSet rs=smt.executeQuery(query);
			
			while(rs.next()) {
				AnnualRainfall annualRainfall=new AnnualRainfall();
				annualRainfall.setCityPincode(rs.getInt(1));
				annualRainfall.setCityName(rs.getString(2));
				annualRainfall.setAverageAnnualRainfall(rs.getDouble(3));
				list.add(annualRainfall);
			}
			con.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;

	}

	public boolean validate(String cityPincode) throws InvalidCityPincodeException {

		String regx = "\\d{5}";
		if (cityPincode.matches(regx))
			return true;
		else {
			throw new InvalidCityPincodeException("Invalid City Pincode");
		}

	}

}
