create table AnnualRainfall(city_pincode int(5) primary key,city_name varchar(25),average_annual_rainfall double(6,2));

insert into AnnualRainfall values(1,'AnithaShalini',95);
insert into AnnualRainfall values(2,'Suvetha',94.5);
insert into AnnualRainfall values(3,'Sreepradha',91.33);
insert into AnnualRainfall values(4,'Viguneshu',72.83);
insert into AnnualRainfall values(5,'Kavinuh',65.83);
insert into AnnualRainfall values(6,'Karthioo',81.83);
insert into AnnualRainfall values(7,'Kishoru',64.83);
insert into AnnualRainfall values(8,'Ranjithu',71.5);
insert into AnnualRainfall values(9,'Dhanya',79.166);